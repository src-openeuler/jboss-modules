%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                jboss-modules
Version:             2.1.2
Release:             1
Summary:             A Modular Classloading System
License:             ASL 2.0 and xpp
URL:                 https://github.com/jbossas/jboss-modules
Source0:             https://github.com/jbossas/jboss-modules/archive/refs/tags/%{namedversion}.tar.gz

BuildArch:           noarch
BuildRequires:       maven-local mvn(junit:junit) mvn(org.jboss:jboss-parent:pom:)
BuildRequires:       mvn(org.jboss.shrinkwrap:shrinkwrap-impl-base)
BuildRequires:       graphviz mvn(jdepend:jdepend) mvn(org.jboss.apiviz:apiviz)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools
%description
Ths package contains A Modular Classloading System.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}-%{namedversion}

%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin :maven-checkstyle-plugin
%pom_remove_plugin :bridger
%pom_remove_plugin org.apache.maven.plugins:maven-surefire-plugin
rm src/test/java/org/jboss/modules/MavenResource2Test.java
rm src/test/java/org/jboss/modules/MavenResourceTest.java 
sed -i '46d' src/test/java/org/jboss/modules/ModuleIteratorTest.java

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc README.md
%license LICENSE.txt XPP3-LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt XPP3-LICENSE.txt

%changelog
* Wed Mar 13 2024 Ge Wang <wang__ge@126.com> - 2.1.2-1
- Update to 2.1.2

* Tue Aug 01 2023 Ge Wang <wang__ge@126.com> - 2.1.0-1
- Update to 2.1.0

* Tue Jun 07 2022 wangkai <wangkai385@h-partners.com> - 2.0.3-1
- Update to 2.0.3

* Fri Sep 5 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 1.5.2-2
- modify Macro
* Wed Aug 26 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 1.5.2-1
- Package init
